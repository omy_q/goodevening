package com.example.goodevening.ui

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.goodevening.R

class GoodEveningStartView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    companion object {
        private const val defaultRatingValue = "0"
    }

    var rating: String = defaultRatingValue
        set(value) {
            updateRating(value)
            field = value
        }

    private val ratingTextView: TextView

    init {
        inflate(context, R.layout.view_start, this)
        ratingTextView = findViewById(R.id.view_star_text)
    }

    private fun updateRating(value: String) {
        ratingTextView.text = value
    }
}